# Maven Local Dependency Example

This repository has two Java projects named Project and Dependency.

It shows how to make a java project (Project) have a dependency to another java project (Dependency) using Maven.

## Make the dependency available on the local Maven repository

To make the dependency available to other projects run the following command on the Dependency project folder.

```sh
$ mvn install
```

## Using the local dependency on any project
To use the dependency on other projects add the project dependency to the projeect pom.xml:

```xml
<dependency>
  <groupId>pt.ipp.isep.dei</groupId>
  <artifactId>maven-dependency-example</artifactId>
  <version>1.0-SNAPSHOT</version>
</dependency>

```

## Possible problems
When the dependency module changes, the package must be installed again on the local Maven repository.
