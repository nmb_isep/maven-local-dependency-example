package pt.ipp.isep.dei.modules.person.model;

public class Name {

    private String name;

    /**
     * Avoid default public constructor with no parameters
     */
    @Deprecated
    private Name() {
    }

    /**
     * Creates a Name value object.
     *
     * @param name Name string
     */
    public Name(String name) {
        name = name;

    }
}
