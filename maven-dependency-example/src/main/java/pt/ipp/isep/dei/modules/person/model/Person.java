package pt.ipp.isep.dei.modules.person.model;

public class Person {

    private Name name;

    /**
     * Avoid default public constructor with no parameters
     */
    @Deprecated
    private Person() {

    }

    /**
     * Creates a Person object.
     * @param name The person's name.
     */
    public Person(String name) {
        this.name = new Name(name);
    }
}
